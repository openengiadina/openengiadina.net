openEngiadina is creating a platform for open local knowledge.

Local knowledge consists of thousands of little pieces of information and data that describe the social, cultural an natural environment of an area.

We believe that local knowledge should be freely available for everyone to use and republish as they wish, local knowledge needs to be [open data](https://opendefinition.org/). However, having access to data is not enough. Data needs to be accessible and efficiently usable. Local knowledge needs to be [open knowledge](https://okfn.org/opendata/).

The openEngiadina project is researching and developing data models, protocols and software that allow local knowledge to be created, shared and used.
