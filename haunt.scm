;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(use-modules (haunt asset)
             (haunt site)
             (haunt post)
             (haunt builder assets)
             (open-engiadina website index)
             (open-engiadina haunt builder atom)
             (haunt reader commonmark))

(define (post-uri site post)
  (post-ref post 'link))

(site #:title "openEngiadina"
      #:domain "openengiadina.net"
      #:default-metadata
      '((author . "openEngiadina")
        (email . "info@openengiadina.net"))
      #:posts-directory "posts"
      #:readers (list commonmark-reader)
      #:builders (list
                  ;; index page with default locale
                  index-page-builder
                  ;; index pages for all languages
                  translated-index-page-builder
                  (atom-feed #:file-name "news.xml"
                             #:subtitle "News on openEngiadina"
                             #:post-uri post-uri)
                  (static-directory "assets" "")
                  (static-directory "papers/" "papers/")))
