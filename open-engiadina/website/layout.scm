;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(define-module (open-engiadina website layout)
  #:use-module (haunt site)
  #:use-module (open-engiadina website i18n)
  #:export (layout))

(define (contact)
  `(div (@ (id "contact") (about "https://openengiadina.net/"))
        (h2 ,(G_ "Contact"))
        (p "Matrix: " (a (@ (property "doap:developer-forum") (href "https://matrix.to/#/#openengiadina:matrix.org")) "#openengiadina:matrix.org"))
        (p ,(string-append  (G_ "E-mail") ": " "info [at] openengiadina [dot] net"))
        (p (a (@ (property "doap:repository") (href "https://gitlab.com/openengiadina/")) "GitLab")
           " "
           (a (@ (href "https://socialhub.activitypub.rocks/c/software/openengiadina/50")) "SocialHub")
           " "
           (a (@ (href "https://wiki.openengiadina.net/")) "Wiki"))))

(define (language-selector)
  (map
   (lambda (lang)
     `(a (@ (href ,(string-append "/" (language-tag lang))))
       ,(language-tag lang)))
   %languages))

(define* (layout site body #:key title)
  `((doctype "html")

    (html (@
           (prefix "og: http://ogp.me/ns# as: https://www.w3.org/ns/activitystreams@ dc: http://purl.org/dc/terms/ schema: http://schema.org/ doap: http://usefulinc.com/ns/doap#"))

          (head
           (meta (@ (charset "utf-8")))
           (link (@ (rel "stylesheet") (type "text/css") (href "/style.css")))
           (link (@ (rel "icon") (type "image/x-icon") (href "/favicon.ico")))
           (link (@ (rel "alternate") (title "openEngiadina news") (href "news.xml") (type "application/atom+xml")))
           (title ,(if title
                       (string-append title " — " (site-title site))
                       (site-title site))))
          (body
           (header (@ (about "https://openengiadina.net/")
                      (typeof "schema:WebPage doap:Project"))

                   (span (@ (style "visiblilty: hidden;") (property "og:title dc:title") (content "openEngiadina")))
                   (span (@ (style "visiblilty: hidden;") (property "og:type") (content "website")))

                   (a (@ (href "index.html"))
                      (img (@ (id "logo") (property "og:image") (about "https://openengiadina.net/") (alt "openEngiadina") (src "/images/open-engiadina.png"))))

                   (p (@ (property "og:description doap:shortdesc")) ,(G_ "Create, share and use open local knowledge.")))

           (nav (@ (class "right-align"))
                ,(language-selector))

           (main ,body)

           (footer ,(contact))))))
