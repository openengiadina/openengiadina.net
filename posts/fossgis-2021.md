title: FOSSGIS 2021
date: 2021-05-25 12:00
link: https://pretalx.com/fossgis2021/talk/QWFKTH/
---
openEngiadina wir in einem Lightning Talk an der [FOSSGIS 2021](https://www.fossgis-konferenz.de/2021/) präsentiert.

An der Konferenz gibt es Möglichkeiten sich über openEngiadina, OpenStreetMap, ActivityPub, RDF, Datalog und andere interessante Sachen auszutauschen. Bis dahin!
