title: Content-addressing and Authenticity
date: 2020-06-11 12:00
link: https://openengiadina.gitlab.io/js-eris/index.html
---
As part of the NLNet project we have been doing [research into data model and data storage](https://gitlab.com/openengiadina/data-model).

We are happy to announce initial [results](./papers/eris.html) and a [demo](https://openengiadina.gitlab.io/js-eris/index.html)!

This enables [content-addressable RDF](./papers/content-addressable-rdf.html) and [ensuring authenticity](./papers/rdf-signify.html) of RDF content, enabling offline-first and decentralized applications - things we are trying to build with openEngiadina.
