title: ActivityPub Conference 2019
date: 2019-09-07 12:00
link: https://conf.tube/videos/watch/81248119-5e19-4798-bc37-f2cd5ed617c7
---
A talk at the [ActivityPub Conference](https://redaktor.me/apconf/) in Prague on how the [ActivityPub](http://activitypub.rocks/) protocol can be used to create any kind of structured data in a crowdsourced manner.
