;; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(use-modules
 (guix packages)
 (guix git-download)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages autotools)
 (gnu packages pkg-config)
 (gnu packages texinfo))

(define-public haunt
  (let ((commit "65adbb052f7d27c382b7f9f3c665635aeab96a02")
        (revision "0"))
    (package
      (name "haunt")
      (version (git-version "0.2.4" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/haunt.git")
                      (commit "65adbb052f7d27c382b7f9f3c665635aeab96a02")))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "19vybz0hczjxj3npznnams5740vqi1gsdsyjiqpy241f783f4i83"))))
      (build-system gnu-build-system)
      (arguments
       `(#:modules ((ice-9 match) (ice-9 ftw)
                    ,@%gnu-build-system-modules)
         #:tests? #f ; test suite is non-deterministic :(
         #:phases (modify-phases %standard-phases
                    (add-after 'install 'wrap-haunt
                      (lambda* (#:key inputs outputs #:allow-other-keys)
                        ;; Wrap the 'haunt' command to refer to the right
                        ;; modules.
                        (let* ((out  (assoc-ref outputs "out"))
                               (bin  (string-append out "/bin"))
                               (site (string-append
                                      out "/share/guile/site"))
                               (guile-reader (assoc-ref inputs "guile-reader"))
                               (deps `(,@(if guile-reader
                                             (list guile-reader)
                                             '())
                                       ,(assoc-ref inputs "guile-commonmark"))))
                          (match (scandir site)
                            (("." ".." version)
                             (let ((modules (string-append site "/" version))
                                   (compiled-modules (string-append
                                                      out "/lib/guile/" version
                                                      "/site-ccache")))
                               (wrap-program (string-append bin "/haunt")
                                 `("GUILE_LOAD_PATH" ":" prefix
                                   (,modules
                                    ,@(map (lambda (dep)
                                             (string-append dep
                                                            "/share/guile/site/"
                                                            version))
                                           deps)))
                                 `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                                   (,compiled-modules
                                    ,@(map (lambda (dep)
                                             (string-append dep "/lib/guile/"
                                                            version
                                                            "/site-ccache"))
                                           deps))))
                               #t)))))))))
      (native-inputs
       `(("autotconf" ,autoconf)
         ("automake" ,automake)
         ("pkg-config" ,pkg-config)
         ("texinfo" ,texinfo)))
      (inputs
       `(("guile" ,guile-3.0)))
      (propagated-inputs
       `(("guile-reader" ,guile-reader)
         ("guile-commonmark" ,guile-commonmark)))
      (synopsis "Functional static site generator")
      (description "Haunt is a static site generator written in Guile
Scheme.  Haunt features a functional build system and an extensible
interface for reading articles in any format.")
      (home-page "http://haunt.dthompson.us")
      (license license:gpl3+))))


(package
  (name "openengiadina.net")
  (version "0.0")
  (source #f)
  (build-system gnu-build-system)
  (inputs
   `(("guile" ,guile-3.0)
     ("haunt" ,haunt)))
  (synopsis #f)
  (description #f)
  (home-page "https://openengiadina.net")
  (license license:gpl3+))
