# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR openEngiadina Authors <https://openengiadina.net>
# This file is distributed under the same license as the openengiadina-website package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: openengiadina-website\n"
"Report-Msgid-Bugs-To: bugs@openengiadina.net\n"
"POT-Creation-Date: 2020-09-20 15:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. TRANSLATORS: News is currently only available in English. It might make sense to indicate that this section is only available in English (e.g. "Neuigkeiten (Englisch)")
#: open-engiadina/website/index.scm:41
msgid "News"
msgstr ""

#: open-engiadina/website/index.scm:47
msgid "Atom feed"
msgstr ""

#: open-engiadina/website/index.scm:51
msgid "Acknowledgments"
msgstr ""

#: open-engiadina/website/index.scm:83
msgid "openEngiadina is developing a platform for open local knowledge."
msgstr ""

#: open-engiadina/website/index.scm:87
msgid "Open Local Knowledge"
msgstr ""

#: open-engiadina/website/index.scm:89
msgid ""
"Local knowledge consists of thousand of little pieces of information that "
"describe the social, cultural and natural environment of an area. For "
"example:"
msgstr ""

#: open-engiadina/website/index.scm:94
msgid "Geographic information"
msgstr ""

#: open-engiadina/website/index.scm:98
msgid "Social and cultural events"
msgstr ""

#: open-engiadina/website/index.scm:102
msgid "Organizations and businesses"
msgstr ""

#: open-engiadina/website/index.scm:106
msgid "Infrastructure status"
msgstr ""

#: open-engiadina/website/index.scm:108
msgid ""
"We believe that local knowledge needs to be freely available for everyone to "
"use and republish as they wish. Local knowledge needs to be open knowledge."
msgstr ""

#: open-engiadina/website/index.scm:112
msgid "Semantic Social Network"
msgstr ""

#: open-engiadina/website/index.scm:114
msgid ""
"openEngiadina is an acknowledgment that data creation and curation is a "
"social activity. We are developing a mashup between a knowledge base (like "
"Wikipedia) and a social network using the ActivityPub protocol and the "
"Semantic Web."
msgstr ""

#: open-engiadina/website/index.scm:121
msgid "For whom and why?"
msgstr ""

#: open-engiadina/website/index.scm:123
msgid ""
"openEngiadina is being developed for small municipalities and local "
"organizations."
msgstr ""

#: open-engiadina/website/index.scm:125
msgid ""
"We want to empower communities to manage their own data. The platform is "
"Free Software, self-hostable and can interoperate with other instances."
msgstr ""

#: open-engiadina/website/index.scm:130
msgid "Free software"
msgstr ""

#: open-engiadina/website/index.scm:134
msgid "Self-hostable"
msgstr ""

#: open-engiadina/website/index.scm:138
msgid "Decentralized"
msgstr ""

#. TRANSLATORS: This is a link to the wiki which is currently only available in English. It might make sense to indicate that (e.g. "Mehr lesen (English) ...").
#: open-engiadina/website/index.scm:142
msgid "Read more..."
msgstr ""

#: open-engiadina/website/layout.scm:12
msgid "Contact"
msgstr ""

#: open-engiadina/website/layout.scm:14
msgid "E-mail"
msgstr ""

#: open-engiadina/website/layout.scm:52
msgid "Create, share and use open local knowledge."
msgstr ""
